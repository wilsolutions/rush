//
//  ViewController.m
//  Rush
//
//  Created by Wilson Gomes on 2017-03-14.
//  Copyright © 2017 WG. All rights reserved.
//

#import "ViewController.h"
#import "CustomTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailsViewController.h"

@interface ViewController ()
@property (nonatomic, weak) IBOutlet UITableView *contentTableView;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, assign) NSInteger totalItems;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.contentTableView.dataSource = self;
    self.contentTableView.delegate = self;

    self.items = [[NSMutableArray alloc]init];

    [self fetchData];
}

- (void)parseData:(NSDictionary *)data {
    NSMutableArray *selectedItems = [NSMutableArray array];
    int totalItems = 0;
    int maxItems = 20;
    NSDictionary *x;

    for (NSDictionary *item in [data valueForKey:@"RelatedTopics"]) {
        if ([item objectForKey:@"FirstURL"]) {
            [selectedItems addObject:item];
            totalItems++;
            // check if it does not exceed the max limit...
            if (totalItems >= maxItems) {
                break;
            }
        }
    }
    // add a few more items in case we have room...
    if (totalItems < maxItems) {
        // todo: to DRY
        for (x in [data valueForKey:@"RelatedTopics"]) {
            if ([x objectForKey:@"Topics"]) {
                for (NSDictionary *item in [x valueForKey:@"Topics"]) {
                    if ([item objectForKey:@"FirstURL"]) {
                        [selectedItems addObject:item];
                        totalItems++;
                        // check if it does not exceed the max limit...
                        if (totalItems >= maxItems) {
                            break;
                        }
                    }
                }
            }
            if (totalItems >= maxItems) {
                break;
            }
        }
    }
    self.items = selectedItems;
    self.totalItems = self.items.count;
    [self reloadData];
}

- (void)reloadData {
    [self.contentTableView reloadData];
}

- (void) showAlert {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:@"Sorry" // todo: to use NSLocalizedString
                                message:@"Could not fetch the data."
                                preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];

                         }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Fetch data
- (void)fetchData {
    // Note: Could use AFNetworking or another network library...
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:@"https://api.duckduckgo.com/?q=apple&format=json&pretty=1"];

    [[session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                [self showAlert];
            } else {
                NSError *parseError = nil;
                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                   options:NSJSONReadingAllowFragments
                                                                                     error:&parseError];
                [self parseData:responseDictionary];
            }
        });
    }] resume];
}

#pragma mark - TableView DataSource Delegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.totalItems;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CustomTableViewCell";

    CustomTableViewCell *cellItem = (CustomTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cellItem == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cellItem = (CustomTableViewCell *)[nib objectAtIndex:0];
    }

    if ([self.items count] > 0) {
        cellItem.title.text = [[self.items objectAtIndex:indexPath.row] valueForKey:@"Text"];

        // todo: to use AFNetworking for better caching support
        [cellItem.icon sd_setImageWithURL:[NSURL URLWithString:[[[self.items objectAtIndex:indexPath.row] valueForKey:@"Icon"] valueForKey:@"URL"]]
                         placeholderImage:[UIImage imageNamed:@"Placeholder"]];
    }

    return cellItem;
}

- (void)tableView:(UITableView *)tv didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"details" sender:self];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.contentTableView indexPathForSelectedRow];
    DetailsViewController *detailsViewController = segue.destinationViewController;
    detailsViewController.url = [NSURL URLWithString:[[self.items objectAtIndex:indexPath.row] valueForKey:@"FirstURL"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

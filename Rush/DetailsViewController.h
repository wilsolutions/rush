//
//  DetailsViewController.h
//  Rush
//
//  Created by Wilson Gomes on 2017-03-14.
//  Copyright © 2017 WG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) NSURL *url;

@end

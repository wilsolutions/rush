//
//  DetailsViewController.m
//  Rush
//
//  Created by Wilson Gomes on 2017-03-14.
//  Copyright © 2017 WG. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()
//@todo: to use WKWebView
@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end

@implementation DetailsViewController

@synthesize url;

- (void)viewDidLoad {
    [super viewDidLoad];

    self.webview.delegate = self;

    NSURLRequest *request = [NSURLRequest requestWithURL:self.url];
    [self.webview setScalesPageToFit:YES];
    [self.webview loadRequest:request];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"Error : %@",error);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
